﻿
var App = angular.module('MyApp', []);
App.controller('ContactCtl', function ($scope, ContactService) { 
    $scope.isFormValid = false;
    $scope.message = null;
    $scope.contact = null;
    var init = function () {
        ContactService.GetContactList().then(function (d) {
            $scope.contact = d.data;
        },
        function () {
            alert('Failed');
        });
    }
    init();
    $scope.Contact = {
        Id: '',
        FirstName: '',
        LastName: '',
        Address: ''
    };
    
    $scope.$watch('CreateForm.$valid', function (newValue) {  
        $scope.isFormValid = newValue;
    });

    
    $scope.AddContact = function (data) {
        $scope.message = '';
        $scope.Contact = data;
        if ($scope.isFormValid) {
            ContactService.AddContact($scope.Contact).then(function (d) {  
                alert(d);
                if (d == 'Success') {
                   
                    ClearForm();
                }
            });
        }
        else {
            $scope.message = 'Please fill the required fields';
        }
    };

     $scope.DelContact = function (cont) {
        if (cont.Id != null) {
            if (window.confirm('Are you sure you want to delete this Id = ' + cont.Id + '?')) 
            {
                ContactService.DelContactData(cont.Id).then(function (cont) {  
                    window.location.href = '/Contact/Index';
                }, function () {
                    alert("Error in deleting record");
                });
            }
        }
        else {
            alert("this id is null");
        }
    };

     $scope.UpdateContact = function () {
        var Contact = {};
        Contact["Id"] = $scope.ContactId;
        Contact["FirstName"] = $scope.ContactFirstName;
        Contact["LastName"] = $scope.ContactLastName;
        Contact["Address"] = $scope.ContactAddress;
        ContactService.UpdateContactData(Contact);
    }

     $scope.RedirectToEdit = function (cont) {
        window.location.href = '/Contact/Edit/' + cont.Id;
    };

     $scope.RedirectToDetails = function (cont) {
        window.location.href = '/Contact/Detail/' + cont.Id;
    };

     ContactService.GetContactByID().success(function (data) {
        $scope.contact = data;
        $scope.ContactId = data.Id;
        $scope.ContactFirstName = data.FirstName;
        $scope.ContactLastName = data.LastName;
        $scope.ContactAddress = data.Address;
  
    });

    //Clear Form Funciton
    function ClearForm() {
        $scope.Contact = {};
        $scope.CreateForm.$setPristine();
        $scope.UpdateCont = {};
        $scope.EditForm.$setPristine();
    }
});
App.factory('ContactService', function ($http, $q, $window) { 
    return {
         GetContactList: function () {
             return $http.get('/Contact/GetAllContacts');
        },

        GetContactId: function () { 
            var urlPath = $window.location.href;
            var result = String(urlPath).split("/");  
            if (result != null && result.length > 0) {
                var id = result[result.length - 1];  
                return id;
            }
        },

        GetContactByID: function () {  
            var currentContactID = this.GetContactId(); 
            if (currentContactID != null) {
                return $http.get('/Contact/GetContactById', { params: { id: currentContactID } });
            }
        },

         AddContact: function (data) {
            var defer = $q.defer();
            $http({
                url: '/Contact/Create',
                method: "POST",
                data: data
            }).success(function (d) {
                 defer.resolve(d);
            }).error(function (e) {
                 alert("Error");
                window.location.href = '/Contact/Create';
                defer.reject(e);
            });
            return defer.promise;
        }, 

         DelContactData: function (contactid) {
            var defer = $q.defer();  
            $http({
                url: '/Contact/Delete/' + contactid,
                method: 'POST'
            }).success(function (d) {
                alert("contact deleted successfully");
                defer.resolve(d);
            }).error(function (e) {
                alert("Error");
                defer.reject(e);
            });
            return defer.promise;

        },

         UpdateContactData: function (contact) {
            var defer = $q.defer();
            contact.Id = this.GetContactId();
            $http({
                url: '/Contact/Update',
                method: 'POST',
                data: contact
            }).success(function (d) {
                defer.resolve(d);
                window.location.href = '/Contact/Index';
            }).error(function (e) {
                alert("Error");
                defer.reject(e);
            });
            return defer.promise;
        },
    }
});
