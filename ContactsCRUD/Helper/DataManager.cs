﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using ContactsCRUD.Models;
using ContactsCRUD.Repositorys;

namespace ContactsCRUD.Helper
{
    public class DataManager
    {
       

        public static void CreateData()
        {
            IRepository databaserepository = new Repository();
            Contact c1 = new Contact();
            c1.FirstName = "Ivan";
            c1.LastName = "Mitić";
            c1.Address = "Kralja Tomislava 5 Zagreb";
            databaserepository.AddContact(c1);
            int contactid1 = databaserepository.GetLastContactId();
            ContactDetail cd1 = new ContactDetail();
            cd1.ContactId = contactid1;
            cd1.ContactType = "Phone";
            cd1.ContactValue = "099988888888";
            databaserepository.AddContactDetail(cd1);
            ContactDetail cd2 = new ContactDetail();
            cd2.ContactId = contactid1;
            cd2.ContactType = "Email";
            cd2.ContactValue = "ivo@vip.hr";
            databaserepository.AddContactDetail(cd2);

            Contact c2 = new Contact();
            c2.FirstName = "Mile";
            c2.LastName = "Dekic";
            c2.Address = "Kralja Petra 5 Zagreb";
            databaserepository.AddContact(c2);
            int contactid2 = databaserepository.GetLastContactId();
            ContactDetail cd3 = new ContactDetail();
            cd3.ContactId = contactid2;
            cd3.ContactType = "Phone";
            cd3.ContactValue = "0984444444";
            databaserepository.AddContactDetail(cd3);
            ContactDetail cd4 = new ContactDetail();
            cd4.ContactId = contactid2;
            cd4.ContactType = "Email";
            cd4.ContactValue = "mile@vip.hr";
            databaserepository.AddContactDetail(cd4);

            Contact c3 = new Contact();
            c3.FirstName = "Marina";
            c3.LastName = "Jelić";
            c3.Address = "Vukovarska 55 Dubrovnik";
            databaserepository.AddContact(c3);
            int contactid3 = databaserepository.GetLastContactId();
            ContactDetail cd5 = new ContactDetail();
            cd5.ContactId = contactid3;
            cd5.ContactType = "Phone";
            cd5.ContactValue = "092334455566";
            databaserepository.AddContactDetail(cd5);
            ContactDetail cd6 = new ContactDetail();
            cd6.ContactId = contactid3;
            cd6.ContactType = "Email";
            cd6.ContactValue = "marina@vip.hr";
            databaserepository.AddContactDetail(cd6);
        }
    }
}