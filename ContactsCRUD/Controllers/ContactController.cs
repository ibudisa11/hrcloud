﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ContactsCRUD.Models;
using ContactsCRUD.Repositorys;
using ContactsCRUD.Helper;

namespace ContactsCRUD.Controllers
{
    public class ContactController : Controller
    {
        private IRepository datarepository = new Repository();

        // GET: Contact
        public ActionResult Index()
        {
            var contactslist = datarepository.GetAllContacts();
            if (contactslist.Count == 0)
            {
                DataManager.CreateData();
            }
            return View();
        }
        //GET
        [HttpGet]
        public JsonResult GetAllContacts()
        {
            var contacts = datarepository.GetAllContacts();
            return Json(contacts, JsonRequestBehavior.AllowGet);
        }
        //GET
        [HttpGet]
        public JsonResult GetContactById(string id)
        {
            var contact = datarepository.GetContactById(id);
            if (contact != null)
            {
                return Json(contact, JsonRequestBehavior.AllowGet);
            }

            else
            {
                return Json(new { Status = "Failure" });

            }
        }


        [HttpGet]
        public JsonResult GetContactDetailsById(string id)
        {
            var contact = datarepository.GetContactDetailsById(id);
            if (contact != null)
            {
                return Json(contact, JsonRequestBehavior.AllowGet);
            }

            else
            {
                return Json(new { Status = "Failure" });

            }
        }

        //public ActionResult Create()
        //{
        //    return View();
        //}

        [HttpPost]
        public JsonResult Create(Contact c)
        {
            string message = "";
            datarepository.AddContact(c);
               
            return Json(message, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult Delete(string id)
        {
            datarepository.DeleteContact(id);
            
            return Json("deleted", JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public ActionResult Edit()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Update(Contact contact)
        {
            string message = "";
            if (contact != null && contact.Id > 0)
            {
                datarepository.UpdateContact(contact);
            }
            return Json(new { Status = "Failure" });
        }

       [HttpGet]
       public JsonResult GetContactsByParameter(string filter,string value)
       {
            List<Contact> list = new List<Contact>();
            list = datarepository.GetContactsByName(filter, value);

            return Json(list, JsonRequestBehavior.AllowGet);
       }

        [HttpPost]
        public JsonResult CreateDetail(ContactDetail c)
        {
            string message = "";
            datarepository.AddContactDetail(c);

            return Json(message, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public JsonResult DeleteDetail(string id)
        {
            datarepository.DeleteContactDetail(id);

            return Json("deleted", JsonRequestBehavior.AllowGet);
        }


        [HttpPost]
        public JsonResult UpdateDetail(ContactDetail contactdetail)
        {
            string message = "";
            if (contactdetail != null && contactdetail.Id > 0)
            {
                datarepository.UpdateContactDetail(contactdetail);
            }
            return Json(new { Status = "Failure" });
        }

        [HttpGet]
        public JsonResult CheckDetails(string id)
        {
            string value = String.Empty;
            bool hasDetails = datarepository.CheckDetails(id);

            if(hasDetails)
            {
                value = "HasDetails";
            }

            return Json(value,JsonRequestBehavior.AllowGet);
        }

        [HttpGet]
        public JsonResult HasMasterData(string id)
        {
            string data = "";
            bool masterdata = false;
            masterdata = datarepository.MasterExists(id);
            if(!masterdata)
            {
                data = "NoData";
            }

            return Json(data, JsonRequestBehavior.AllowGet);
        }
    }
}