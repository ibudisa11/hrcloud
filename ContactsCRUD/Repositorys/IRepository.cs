﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ContactsCRUD.Models;

namespace ContactsCRUD.Repositorys
{
    public interface IRepository
    {
        List<Contact> GetAllContacts();
        Contact GetContactById(string id);
        void AddContact(Contact contact);
        void UpdateContact(Contact contact);
        void DeleteContact(string id);
        List<Contact> GetContactsByName(string filter, string value);
        List<ContactDetail> GetContactDetailsById(string contactid);
        void AddContactDetail(ContactDetail detail);
        void UpdateContactDetail(ContactDetail detail);
        void DeleteContactDetail(string id);
        bool CheckDetails(string contactid);
        bool MasterExists(string id);
        int GetLastContactId();
    }
}
