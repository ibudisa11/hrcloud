﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ContactsCRUD.Models;

namespace ContactsCRUD.Repositorys
{
    public class Repository : IRepository
    {
        private AngularEntities con = new AngularEntities();

        public void AddContact(Contact contact)
        {
           
                var cont = con.Contact.Where(a => a.Id == contact.Id).FirstOrDefault();
                if (cont == null)
                {
                    con.Contact.Add(contact);
                    con.SaveChanges();
                   
                }
            
        }

        public void AddContactDetail(ContactDetail detail)
        {
            var contdetail = con.ContactDetail.Where(a => a.Id == detail.Id).FirstOrDefault();
            if (contdetail == null)
            {
                con.ContactDetail.Add(detail);
                con.SaveChanges();

            }
        }

        public bool CheckDetails(string contactid)
        {
            bool contains = false;
            List<ContactDetail> details = new List<ContactDetail>();
            int number = int.Parse(contactid);

            details = con.ContactDetail.Where(m => m.ContactId == number).ToList();

            int count = details.Count;
            if(count>0)
            {
                contains = true;
            }
            return contains;
        }

        public void DeleteContact(string id)
        {
            Contact contact;
             
                int k = int.Parse(id);
                contact = con.Contact.Where(a => a.Id == k).SingleOrDefault();
                if (contact != null)
                {
                    con.Contact.Remove(contact);
                    con.SaveChanges();

                }
        }

        public void DeleteContactDetail(string id)
        {
            int k = int.Parse(id);
            ContactDetail  contactdetail = con.ContactDetail.Where(a => a.Id == k).SingleOrDefault();
            if (contactdetail != null)
            {
                con.ContactDetail.Remove(contactdetail);
                con.SaveChanges();

            }
        }

        public List<Contact> GetAllContacts()
        {
            List<Contact> contacts = new List<Contact>();
            contacts = con.Contact.ToList();
            return contacts;
        }

        public Contact GetContactById(string id)
        {
            Contact contact;
            int number = int.Parse(id);
 
            contact = con.Contact.SingleOrDefault(m => m.Id == number);
            
            return contact;
        }

        public List<ContactDetail> GetContactDetailsById(string contactid)
        {
            List<ContactDetail> details = new List<ContactDetail>();
            int number = int.Parse(contactid);

            details = con.ContactDetail.Where(m => m.ContactId == number).ToList();
          
            return details;
        }

        public List<Contact> GetContactsByName(string filter, string value)
        {
            List<Contact> contacts = new List<Contact>();
            switch (filter)
            {
                case "FirstName":
                    contacts = (from c in con.Contact
                           where c.FirstName.StartsWith(value)
                           select c).ToList();
                    break;
                case "LastName":
                    contacts = (from c in con.Contact
                                where c.LastName.StartsWith(value)
                                select c).ToList();
                    break;
                case "Address":
                    contacts = (from c in con.Contact
                                where c.Address.StartsWith(value)
                                select c).ToList();
                    break;
            }

            return contacts;
        }

        public int GetLastContactId()
        {
           int maxid = 0;
            int length = con.Contact.ToList().Count;
            if (length > 0)
            {
                maxid = con.Contact.Max(d => d.Id);
            }
            return maxid;
        }

        public bool MasterExists(string id)
        {
            bool existsMaster = false;
            int n = int.Parse(id);
            Contact c = con.Contact.Where(m => m.Id == n).SingleOrDefault();
            if(c!=null)
            {
                existsMaster = true;
            }

            return existsMaster;
        }

        public void UpdateContact(Contact contact)
        {
            if (contact != null && contact.Id > 0)
            {
                var CurrentContact = con.Contact.Where(a => a.Id == contact.Id).SingleOrDefault();
                if (CurrentContact != null)
                {

                    CurrentContact.FirstName = contact.FirstName;
                    CurrentContact.LastName = contact.LastName;
                    CurrentContact.Address = contact.Address;

                    con.Contact.Attach(CurrentContact);
                    con.Entry(CurrentContact).State = EntityState.Modified;
                    con.SaveChanges();
                }
                 
            }
        }

        public void UpdateContactDetail(ContactDetail detail)
        {
            if (detail != null && detail.Id > 0)
            {
                var CurrentContactDetail = con.ContactDetail.Where(a => a.Id == detail.Id).SingleOrDefault();
                if (CurrentContactDetail != null)
                {

                    CurrentContactDetail.ContactId = detail.ContactId;
                    CurrentContactDetail.ContactType = detail.ContactType;
                    CurrentContactDetail.ContactValue = detail.ContactValue;

                    con.ContactDetail.Attach(CurrentContactDetail);
                    con.Entry(CurrentContactDetail).State = EntityState.Modified;
                    con.SaveChanges();
                }

            }
        }
    }
}
